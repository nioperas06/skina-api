# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 03:13:59 2018

@author: HP
"""

import cv2 as cv
import numpy as np

from Tools import Tools

class Eval_Pyt(Tools):

    def __init__(self):
        Tools.__init__(self)
        self.list_ref = [('Moyenne gris', 108.40, 60), ('Moyenne rouge', 164.3, 20), ('Moyenne bleu', 86.52, 40), ('Moyenne vert', 110.37, 30), ('Asymétrie', 0.08173, (0.08173 - 0.024)), ('Moyenne contour', 10, -10)]
        self.dicti = {}
        self.percent_2 = 0
        self.comp = 0

    def active_tools_dict(self, img):
        self.treat_img_param(self.add_images(img, self.thresh_otsu(img)))
        self.dicti = self.show_param(img)
        #return self.show_param(img)

    def active_tools_dict_r(self, img):
        self.treat_img_param(self.add_images(img, self.thresh_otsu(img)))
        self.dicti = self.show_param(img)
        return self.dicti

    def cal_percent_Pyt(self):
        self.percent_2 = 0
        for j in range(6):
            if (self.list_ref[j][0] in self.dicti.keys()):
                if (self.dicti[self.list_ref[j][0]] <= (self.list_ref[j][1] + self.list_ref[j][2])) and (self.dicti[self.list_ref[j][0]] >= (self.list_ref[j][1] - self.list_ref[j][2])):
                    self.percent_2 = self.percent_2 + 15
                    self.comp = self.comp + 1

                if (j == 5) and (self.dicti[self.list_ref[j][0]] <= 10):
                    self.percent_2 = self.percent_2 + 10

        #print(self.comp)
        return ('Pytiriasis Alba', self.percent_2)

    def result_show(self):
        chain = '{} {}%'.format('Le pourcentage de diagnostic pour le Pytiriasis Alba est de', self.cal_percent_Pyt())
        print(self.dicti)
        print(chain)

    def getPercent_Pyt(self):
        return self.cal_percent_Pyt()

#try:
#    im = cv.imread('vit25.jpg')
#
#    Eval = Eval_Pyt()
#    Eval.active_tools_dict(im)
#    Eval.result_show()
#    Eval.img_plot_scheme(im)
#
#except:
#    print('ERROR TO EXECUTE')