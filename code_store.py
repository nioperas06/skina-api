# -*- coding: utf-8 -*-
"""
Created on Wed Nov 07 15:29:44 2018

@author: HP
"""

import numpy as np 
import cv2 
from mss import mss 
from PIL import Image 

mon = {'top': 160, 'left': 160, 'width': 200, 'height': 200} 

sct = mss() 

while 1: 
    sct.get_pixels(mon) 
    img = Image.frombytes('RGB', (sct.width, sct.height), sct.image) 
    cv2.imshow('test', np.array(img)) 
    if cv2.waitKey(25) & 0xFF == ord('q'): 
     cv2.destroyAllWindows() 
     break
 
#****************************************************************************************************************************
     

plt.imshow(cv.add(V))

plt.subplot(321), img_o.show_cv_plt(orig)
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(322), img_o.show_cv_plt(img_o.remove_noise(orig))
plt.title('Noise removed Image'), plt.xticks([]), plt.yticks([])
plt.subplot(323),plt.imshow(img_o.detect_edges(orig), cmap='gray')
plt.title('Edges detection Image'), plt.xticks([]), plt.yticks([])
#plt.subplot(224), img_o.show_cv_plt(img_o.thresh_otsu(orig))
plt.subplot(324), plt.imshow(img_o.thresh_otsu(orig), cmap='gray')
plt.title('Otsu thresholding Image'), plt.xticks([]), plt.yticks([])
plt.subplot(325), img_o.show_cv_plt(img_o.add_images(orig, img_o.thresh_otsu(orig)))
plt.title('Sample obviousness'), plt.xticks([]), plt.yticks([])
plt.subplot(326), img_o.show_cv_plt(img_o.add_images(orig, img_o.thresh_otsu(orig)), 1)
plt.title('Sample gray obviousness'), plt.xticks([]), plt.yticks([])