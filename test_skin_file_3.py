# -*- coding: utf-8 -*-
"""
Created on Mon Nov 05 18:10:14 2018

@author: HP
"""

import cv2
from matplotlib import pyplot as plt

img = cv2.imread('Pi_2.png')
edges = cv2.Canny(img,100,200)

plt.subplot(121),plt.imshow(img, cmap = 'gray')
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(edges)
plt.title('Edge Image'), plt.xticks([]), plt.yticks([])

plt.show()

#kernel = np.ones((3, 3), np.uint8)
#closing = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel, iterations = 2)
#bg = cv2.dilate(closing, kernel, iterations = 1)
#dist_transform = cv2.distanceTransform(closing, cv2.DIST_L2, 0)
#ret, fg = cv2.threshold(dist_transform, 0.02 * dist_transform.max(), 255, 0)

b, g, r = cv2.split(img)

res_i = 0.6099*r+0.5953*g+0.05231*b;

plt.imshow(res_i)

cv2.imshow('resultat', res_i)
type(res_i)
