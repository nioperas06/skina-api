# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 14:01:08 2018

@author: HP
"""

import cv2 as cv
import numpy as np

from Evaluator_Pytiriasis_Alba import Eval_Pyt
from Evaluator_Vitiligo import Eval_Vit

import base64
from PIL import Image
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

from io import BytesIO

obj_vi = Eval_Vit()
obj_py = Eval_Pyt()

def data_uri_to_cv2_img(uri):
    encoded_data = uri.split(',')[1]
    nparr = np.fromstring(encoded_data, np.uint8)
    img = cv.imdecode(nparr, cv.IMREAD_COLOR)
    return img

def diagnostics(img_name):
    vit_perc = 0
    pyt_perc = 0
    try :
        file = img_name
        starter = file.find(',')
        image_data = file[starter+1:]
        image_data = bytes(image_data, encoding="ascii")
        im = Image.open(BytesIO(base64.b64decode(image_data)))
        im.save('image.jpg')

        # decoded_image = data_uri_to_cv2_img(data_uri)
        img = cv.imread('image.jpg')

        print('Paramètres de diagnostics')
        print(obj_vi.active_tools_dict_r(img))
        obj_py.active_tools_dict(img)
        #self.result_show()
        vit_perc = obj_vi.cal_percent_Vit()[1]
        pyt_perc = obj_py.cal_percent_Pyt()[1]
        print(vit_perc, pyt_perc)
        if (vit_perc > pyt_perc):
            dec = 'Vitiligo détecté'
        elif (vit_perc < pyt_perc):
            dec = 'Pytiriasis Alba détecté'
        else:
            print('Essayez une autre photo')

        obj_vi.img_plot_scheme(img)

        return dec
    except:
        print('ERROR TO EXECUTE')

diagnostics('gr_sis.jpg')
