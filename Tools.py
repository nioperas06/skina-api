# -*- coding: utf-8 -*-
"""
Created on Tue Nov 06 01:17:15 2018

@author: HP
"""
import cv2  as cv
import numpy as np
import matplotlib.pyplot as plt
from math import sqrt

class Tools(object):
    def __init__(self):
        self.h = 0
        self.w = 0
        self.summ = 0
        self.summ_asy = 0
        self.ent = 0
        self.dico = {}

    # fonction pour convertir un tuple en liste
    def conv_tup_lst(self, tup_v):
        "convert tuple to list"
        return list(tup_v)
     # calcul de la moyenne de niveaux de gris
    def cal_moy(self, img):
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        #self.dico['Moyenne gris'] = gray_img.mean()
        return gray_img.mean()

    # calcul de l'ecart-type
    def cal_ecart_t(self, img):
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        self.h, self.w = self.conv_tup_lst(gray_img.shape)[0], self.conv_tup_lst(gray_img.shape)[1]
        for i in range(self.h):
            for j in range(self.w):
                self.ent = self.ent + ((gray_img[i][j]**2)/(self.h * self.w))
        ec_t = sqrt(abs(self.ent - self.cal_moy(img)**2))
        return ec_t

    def cal_asy(self, img):
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        self.h, self.w = self.conv_tup_lst(gray_img.shape)[0], self.conv_tup_lst(gray_img.shape)[1]
        for i in range(self.h):
            for j in range(self.w):
                self.summ_asy = self.summ_asy + ((gray_img[i][j] - self.cal_moy(img))**3)
        interm = (self.h * self.w)/ ((self.h * self.w - 1)*(self.w * self.h - 2)*(self.cal_ecart_t(img))**3)
        return round(self.summ_asy * interm, 6)

    def cal_unif(self, img):
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        self.h, self.w = self.conv_tup_lst(gray_img.shape)[0], self.conv_tup_lst(gray_img.shape)[1]
        for i in range(self.h):
            for j in range(self.w):
                self.summ = self.summ + ((gray_img[i][j])**2)/(255**2)
        return self.summ

    def cal_min(self, img):
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        return gray_img.min()

    def cal_max(self, img):
        gray_img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        #self.dico['Maximum gris'] = gray_img.max()
        return gray_img.max()



    # calcul de la moyenne par couleur
    def aver_col(self, img):
        average_color = [img[:,:,i].mean() for i in range(img.shape[-1])]
        return average_color

    # image couleur de b,g,r vers r,g,b
    def img_ver_val(self, img):
        b, g, r = cv.split(img)
        img_2 = cv.merge([r,g,b])
        return img_2

    # application du contraste
    def img_clahe(self, img):
        gray= cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
        res = clahe.apply(gray)
        cv.imwrite('clahe_2.jpg',res)
        return res

    def img_gray_type(self, img):
        return self.img_clahe(self.img_ver_val(img))

    #fonction exploitant le module plot de matplotlib pour afficher une image opencv en corrigeant le b,g,r
    #en r,g,b par défaut
    def show_cv_plt(self, img, g_i=0):
        try:
            if g_i == 0:
                b, g, r = cv.split(img)
                img_2 = cv.merge([r,g,b])
                plt.imshow(img_2)
            else:
                plt.imshow(img)
        except:
            print('Error to plot')

    #fonction pour afficher l'histogramme en précisant ce qui doit être affiché en gris ou en couleur.
    def hist_show_gray(self, img, g_i=1, y_sup_bd = 5000):
        "plot histogram for color and gray images"
        #img = cv.imread(file_path)
        try:
            if g_i == 0:
                gray= cv.cvtColor(img, cv.COLOR_BGR2GRAY)
                plt.hist(gray.ravel(), 256, [0, 256])
                plt.xlim([0, 256])
                plt.ylim([0, y_sup_bd])
                plt.show()
            else:
                color = ('b','g','r')
                for i, col in enumerate(color):
                    histr = cv.calcHist([img], [i], None, [256], [0, 256])
                    plt.plot(histr, color = col)
                    plt.xlim([0, 256])
                    plt.ylim([0, y_sup_bd])
                plt.show()
        except:
            print("Erreur d'affichage")

    # show only skin  image with pyplot
    def skin_detector_plot(self, img):
        #img = cv.imread(img_path)
        img_conv = cv.cvtColor(img, cv.COLOR_BGR2HSV)

        #relative to HSV boundaries of skin
        lower = np.array([0, 48, 80], dtype="uint8")
        upper = np.array([20, 255, 255], dtype="uint8")

        skinMask = cv.inRange(img_conv, lower, upper)

        skinMask = cv.GaussianBlur(skinMask, (3, 3), 0)
        skin = cv.bitwise_and(img, img, mask = skinMask)
        return skin

    def thresh_otsu(self, img):
        gray= cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        ret, imgf = cv.threshold(gray, 0, 255, cv.THRESH_OTSU + cv.THRESH_BINARY)
        #self.show_cv_plt(imgf, 1)
        return imgf

    def remove_noise(self, img):
        dst = cv.fastNlMeansDenoisingColored(img, None,10,10,7,21)
        return dst

    # détecter les contours des symptômes
    def detect_edges(self, img):
        edges = cv.Canny(img, 100, 200)
        return edges

    def add_images(self, img1, img2):
        img_2 = cv.merge([img2,img2,img2]) # On crée une image rgb à 3 composantes
        #res = cv.add(img1, img_2)
        res = cv.subtract(img1, img_2)
        res = cv.add(res, img1)
        return res

    def treat_img_param(self, img):
        self.dico['Minimum gris'] = round(self.cal_min(img), 2)
        self.dico['Maximum gris'] = round(self.cal_max(img), 2)
        self.dico['Ecart-type gris'] = round(self.cal_ecart_t(img), 2)
        self.dico['Asymetrie'] = self.cal_asy(img)
        self.dico['Moyenne bleu'] = round(self.aver_col(img)[0], 2)
        self.dico['Moyenne vert'] = round(self.aver_col(img)[1], 2)
        self.dico['Moyenne rouge'] = round(self.aver_col(img)[2], 2)

    def show_param(self, img):
        self.dico['Moyenne gris'] = round(self.img_gray_type(img).mean(), 2)
        self.dico['Moyenne contour'] = round(self.detect_edges(img).mean(), 2)
        return self.dico

    def img_lesion_evidence(self, img1, img2):
        # I want to put logo on top-left corner, So I create a ROI
        img2gray = cv.cvtColor(img2,cv.COLOR_BGR2GRAY)
        #h, w =  self.conv_tup_lst(img2gray)[0], self.conv_tup_lst(img2gray)[1]
        roi = img1
        # Now create a mask of logo and create its inverse mask also
        ret, mask = cv.threshold(img2gray, 10, 255, cv.THRESH_BINARY + cv.THRESH_OTSU)
        mask_inv = cv.bitwise_not(mask)
        # Now black-out the area of logo in ROI
        img1_bg = cv.bitwise_and(roi,roi,mask = mask_inv)
        # Take only region of logo from logo image.
        img2_fg = cv.bitwise_and(img2,img2,mask = mask)
        # Put logo in ROI and modify the main image
        dst = cv.add(img1_bg,img2_fg)
        return dst

    def img_plot_scheme(self, orig):
        #self.treat_img_param(self.add_images(orig, self.thresh_otsu(orig)))
        #print(self.show_param(orig))

        plt.subplot(421), self.show_cv_plt(orig)
        plt.title('Original image'), plt.xticks([]), plt.yticks([])

        plt.subplot(422), plt.imshow(self.img_gray_type(orig), cmap='gray')
        plt.title('CLAHE gray image'), plt.xticks([]), plt.yticks([])

        plt.subplot(423), self.show_cv_plt(self.remove_noise(orig))
        plt.title('Noise removed Image'), plt.xticks([]), plt.yticks([])

        plt.subplot(424),plt.imshow(self.detect_edges(orig), cmap='gray')
        plt.title('Edges detection Image'), plt.xticks([]), plt.yticks([])

        plt.subplot(425), self.show_cv_plt(self.skin_detector_plot(orig))
        plt.title('Skin detection'), plt.xticks([]), plt.yticks([])

        plt.subplot(426), plt.imshow(self.thresh_otsu(self.remove_noise(orig)), cmap='gray')
        plt.title('Otsu thresholding image'), plt.xticks([]), plt.yticks([])

        plt.subplot(427), self.show_cv_plt(self.add_images(orig, self.thresh_otsu(orig)))
        plt.title('Sample obviousness'), plt.xticks([]), plt.yticks([])

        plt.subplot(428), self.show_cv_plt(self.add_images(orig, self.thresh_otsu(orig)), 1)
        plt.title('Sample gray obviousness'), plt.xticks([]), plt.yticks([])






#Norig = cv.imread('vit25.jpg')
#img_o = Tools()

#print img_o.cal_asy(orig)
#
#img_o.treat_img_param(img_o.add_images(orig, img_o.thresh_otsu(orig)))
#print(img_o.show_param(orig))
#
#plt.subplot(421), img_o.show_cv_plt(orig)
#plt.title('Original image'), plt.xticks([]), plt.yticks([])
#
#plt.subplot(422), plt.imshow(img_o.img_gray_type(orig), cmap='gray')
#plt.title('CLAHE gray image'), plt.xticks([]), plt.yticks([])
#
#plt.subplot(423), img_o.show_cv_plt(img_o.remove_noise(orig))
#plt.title('Noise removed Image'), plt.xticks([]), plt.yticks([])
#
#plt.subplot(424),plt.imshow(img_o.detect_edges(orig), cmap='gray')
#plt.title('Edges detection Image'), plt.xticks([]), plt.yticks([])
#
#plt.subplot(425), img_o.show_cv_plt(img_o.skin_detector_plot(orig))
#plt.title('Skin detection'), plt.xticks([]), plt.yticks([])
#
#plt.subplot(426), plt.imshow(img_o.thresh_otsu(img_o.remove_noise(orig)), cmap='gray')
#plt.title('Otsu thresholding image'), plt.xticks([]), plt.yticks([])
#
#plt.subplot(427), img_o.show_cv_plt(img_o.add_images(orig, img_o.thresh_otsu(orig)))
#plt.title('Sample obviousness'), plt.xticks([]), plt.yticks([])
#
#plt.subplot(428), img_o.show_cv_plt(img_o.add_images(orig, img_o.thresh_otsu(orig)), 1)
#plt.title('Sample gray obviousness'), plt.xticks([]), plt.yticks([])




