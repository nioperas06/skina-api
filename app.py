import os
from flask import Flask, flash, request, redirect, url_for, jsonify
from Dec_val import diagnostics
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

@app.route("/upload", methods=['POST'])
def upload():
    content = request.json
    if content['image']:
        return jsonify(
            message=diagnostics(content['image'])
        )
    return jsonify(
        message="Something went wrong!",
    ), 400
