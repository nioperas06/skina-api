# -*- coding: utf-8 -*-
"""
Created on Sat Nov 03 01:27:21 2018

@author: HP
"""
import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

def conv_tup_lst(tup_v): 
        "convert tuple to list"
        return list(tup_v)
    
def show_cv_plt(img, g_i=0):    
    try:
        if g_i == 0:
            b, g, r = cv.split(img)
            img_2 = cv.merge([r,g,b])
            plt.imshow(img_2)
        else:
            plt.imshow(img)
    except:
        print('Error to plot')
    
def skin_gray(img):
    b, g, r = cv.split(img)
    img_Gray = np.zeros((conv_tup_lst(img.shape)[0], conv_tup_lst(img.shape)[1]))
    for i in range(conv_tup_lst(img.shape)[0]):
        for j in range(conv_tup_lst(img.shape)[1]):
            img_Gray[i][j] = 0.6099 * r[i][j]  + 0.5953 * g[i][j]  + 0.5231 * b[i][j] 
    return img

im_or = cv.imread('Pi_1.png')

plt.imshow(skin_gray(im_or))
    
    